#include "Client.hpp"
#include "Communication.hpp"
#include "GameInstance.hpp"
#include "Player.hpp"
#include <SFML/Network.hpp>
#include <SFML/Network/Socket.hpp>
#include <exception>
#include <iostream>
#include <stdexcept>

Client::Client(const unsigned short port)
    : _port(port)
{
}

ServerMessage::GameState Client::connect(const std::string &username, GameInstance &instance, const std::string &ip)
{
    if (_socket.connect(ip, _port) != sf::Socket::Done)
        throw std::runtime_error("Could not listen on port " +
                                 std::to_string(_port));
    sf::Packet packet;
    packet << ClientMessage::PLAYERINIT;
    packet << username;
    if (_socket.send(packet) != sf::Socket::Done)
        throw std::runtime_error("Could not send Initialization packet");
    instance.getScreen().addToPrompt("Waiting the start of the game...");
    instance.getScreen().printPrompt();
    _socket.setBlocking(true);
    _socket.receive(packet);
    ServerMessage::Launch launch;
    ServerMessage::Log log;
    ServerMessage::Type type;
    packet >> type;
    while (type == ServerMessage::Type::LOG) {
        packet >> log;
        instance.getScreen().addToPrompt("SERVER: " + log);
        instance.getScreen().printPrompt();
        _socket.receive(packet);
        packet >> type;
    }
    if (type != ServerMessage::Type::LAUNCH)
        throw std::exception();
    packet >> launch;
    instance.is_traitor = launch.is_traitor;
    for (unsigned i = 0; i < launch.players.size(); ++i) {
        instance.addPlayer(
            *(new Player(launch.players[i], i, Player::PlayerType::CREWMAN)));
    }
    return launch.state;
}
