#include "Screen.hpp"
#include <curses.h>

Screen::Screen() : _prompt_size(), _prompt_history(), _Map("./assets/map.txt")
{
    Screen::initScreen();
    _prompt_size = (3 * LINES / 10) - 3;
    _prompt_history.reserve(_prompt_size);
    _map = subwin(stdscr, 7 * LINES / 10, 8 * COLS / 10, 0, 0);
    _playerList =
        subwin(stdscr, 7 * LINES / 10, 2 * COLS / 10, 0, 8 * COLS / 10);
    _help = subwin(stdscr, 3 * LINES / 10, 3 * COLS / 10, 7 * LINES / 10,
                   7 * COLS / 10);
    _status = subwin(stdscr, 3 * LINES / 10, 3 * COLS / 10, 7 * LINES / 10,
                     4 * COLS / 10);
    _prompt = subwin(stdscr, 3 * LINES / 10, 4 * COLS / 10, 7 * LINES / 10, 0);
    boxW();
    titleW();
    refreshW();
}

Screen::~Screen()
{
    endwin();
}

void Screen::refreshW()
{
    wrefresh(_help);
    wrefresh(_map);
    wrefresh(_prompt);
    wrefresh(_status);
    wrefresh(_playerList);
}

void Screen::refreshW(ScreenType type)
{
    switch (type) {
    case ScreenType::Help:
        wrefresh(_help);
        break;
    case ScreenType::Status:
        wrefresh(_status);
        break;
    case ScreenType::MapScreen:
        wrefresh(_map);
        break;
    case ScreenType::Prompt:
        wrefresh(_prompt);
        break;
    case ScreenType::PlayerList:
        wrefresh(_playerList);
        break;
    default:
        throw ScreenErr("Not a valid window");
        break;
    }
}

void Screen::titleW()
{
    mvwprintw(_status, 0, 1, "Status");
    mvwprintw(_map, 0, 1, "Map");
    mvwprintw(_prompt, 0, 1, "Prompt");
    mvwprintw(_help, 0, 1, "Help");
    mvwprintw(_playerList, 0, 1, "Player List");
}

void Screen::titleW(ScreenType type)
{
    switch (type) {
        case ScreenType::Help: mvwprintw(_help, 0, 1, "Help"); break;
        case ScreenType::Status: mvwprintw(_help, 0, 1, "Status"); break;
        case ScreenType::MapScreen: mvwprintw(_map, 0, 1, "Map"); break;
        case ScreenType::Prompt: mvwprintw(_prompt, 0, 1, "Prompt"); break;
        case ScreenType::PlayerList: mvwprintw(_playerList, 0, 1, "Player List"); break;
        default: throw ScreenErr("Not a valid window"); break;
    }
}

void Screen::boxW()
{
    box(_help, ACS_VLINE, ACS_HLINE);
    box(_status, ACS_VLINE, ACS_HLINE);
    box(_map, ACS_VLINE, ACS_HLINE);
    box(_prompt, ACS_VLINE, ACS_HLINE);
    box(_playerList, ACS_VLINE, ACS_HLINE);
}

void Screen::boxW(ScreenType type)
{
    switch (type) {
        case ScreenType::Help: box(_help, ACS_VLINE, ACS_HLINE); break;
        case ScreenType::Status: box(_status, ACS_VLINE, ACS_HLINE); break;
        case ScreenType::MapScreen: box(_map, ACS_VLINE, ACS_HLINE); break;
        case ScreenType::Prompt: box(_prompt, ACS_VLINE, ACS_HLINE); break;
        case ScreenType::PlayerList: box(_playerList, ACS_VLINE, ACS_HLINE); break;
        default: throw ScreenErr("Not a valid window"); break;
    }
}

void Screen::drawMap(bool r)
{
    unsigned int y = 0;

    for (y = 0; y < _Map.getMap().size(); y++) {
        for (long unsigned int x = 0; x < _Map.getMap()[y].size(); x++) {
            if (_Map.getMap()[y][x] == 'P')
                wattron(_map, A_BOLD);
            mvwprintw(_map, y + 1, x + 1, "%c", _Map.getMap()[y][x]);
            if (_Map.getMap()[y][x] == 'P')
                wattroff(_map, A_BOLD);
        }
    }
    if (r == true)
        refreshW(ScreenType::MapScreen);
}

std::string Screen::getPromptInput()
{
    char msg[COLS];

    mvprintw(LINES - 3, 2, "> ");
    getstr(msg);
    if (msg[0] != '\n')
        addToPrompt(std::string("> ") + std::string(msg));
    return std::string(msg);
}

void Screen::printPrompt(bool r)
{
    wclear(_prompt);
    boxW(ScreenType::Prompt);
    titleW(ScreenType::Prompt);
    for (long unsigned int i = 0; i < _prompt_history.size(); i++) {
        mvwprintw(_prompt, 1 + i, 2, _prompt_history[i].c_str());
    }
    if (r == true)
        refreshW(ScreenType::Prompt);
}

void Screen::addToPrompt(const std::string &str, bool refresh)
{
    if (_prompt_history.size() >= _prompt_size)
        _prompt_history.erase(_prompt_history.begin());
    _prompt_history.push_back(str);
    if (refresh)
        this->printPrompt();
}

void Screen::printTutorial(bool isTraitor, bool r)
{
    mvwprintw(_help, 1, 1, "You are a %s",
              (isTraitor ? "traitor" : "crew member"));
    mvwprintw(_help, 3, 1, "move [room] - move to room");
    mvwprintw(_help, 5, 1, "ask_vote - vote to put a player in jail");
    mvwprintw(_help, 7, 1, "repair - repair current room");
    if (isTraitor) {
        mvwprintw(_help, 9, 1, "Only for Traitor");
        mvwprintw(_help, 10, 3, "damage - damage current room");
    }
    if (r == true)
        refreshW(ScreenType::Help);
}

void Screen::printPlayerJob(bool &is_traitor)
{
    if (is_traitor) {
        mvwprintw(_status, 3, 3, "You are a traitor");
        mvwprintw(_status, 4, 3, "Destroy the ship !");
    } else {
        mvwprintw(_status, 3, 3, "You are a crewman.");
        mvwprintw(_status, 4, 3, "Fix the ship and capture traitor.");
    }
    mvwprintw(_status, 6, 3, "Press Enter to continue");
    this->addToPrompt("Press Enter to continue", true);
    refreshW(ScreenType::Status);
    getch();
}

void Screen::printStatus(ServerMessage::GameState &status, bool r)
{
    std::string jail =
        std::string((status.turns_in_prison > 0) ? ("Yes") : ("No"));

    wclear(_status);
    boxW(ScreenType::Status);
    mvwprintw(_status, 1, 1, "Current room : %i", status.id_room);
    mvwprintw(_status, 3, 1, "Room health : %i%%", status.roomlife);
    mvwprintw(_status, 5, 1, "In jail ? %s", jail.c_str());
    if (jail == "Yes")
        mvwprintw(_status, 6, 1, "Turns left : %i", status.turns_in_prison);
    mvwprintw(_status, 8, 1, "Storm is %i lightyears away",
              status.storm_distance);
    if (r == true)
        refreshW(ScreenType::Status);
}

Map &Screen::getMap()
{
    return _Map;
}

void Screen::printPlayerList(std::vector<std::string> player, bool r)
{
    int y = 0;

    for (auto &i : player) {
        mvwprintw(_playerList, y + 1, 1, i.c_str());
        y += 2;
    }
    if (r == true)
        refreshW(ScreenType::PlayerList);
}

void Screen::initScreen()
{
    initscr();
    // noecho();
    // curs_set(false);
    clear();
    cbreak();
    keypad(stdscr, true);
}
