
#include "GameInstance.hpp"
#include <algorithm>
#include <sstream>

std::vector<std::string> parse_cmd(std::string cmd)
{
    std::istringstream iss(cmd);
    std::vector<std::string> words;
    copy(std::istream_iterator<std::string>(iss),
         std::istream_iterator<std::string>(), back_inserter(words));
    return words;
}
