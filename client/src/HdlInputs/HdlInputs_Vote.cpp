
#include "Communication.hpp"
#include "HdlInputs_Classic.hpp"
#include <SFML/Network/Socket.hpp>
#include <algorithm>
#include <iterator>
#include <stdexcept>
#include <string>

void HdlInputs_Vote(GameInstance &instance, std::vector<std::string> &cmd)
{
    auto players = instance.getPlayers();

    // Get player pos in player list
    auto player_it =
        std::find_if(players.begin(), players.end(), [cmd](Player &player) {
            return player.getName() == cmd[0];
        });
    // If the player does not exist, return
    if (player_it == players.end())
        return;

    std::size_t player_id = std::distance(players.begin(), player_it);

    // Send the vote
    ClientMessage::PlayerAction action;
    sf::Packet packet;
    packet << ClientMessage::Type::PLAYERACTION;
    action.type = ClientMessage::PlayerAction::ActionType::VOTE;
    action.data = player_id;
    packet << action;
    if (instance.getClient()._socket.send(packet) != sf::Socket::Done)
        throw std::runtime_error("Could not send vote request");
    return;
}
