
#include "HdlInputs_Classic.hpp"
#include "Communication.hpp"
#include <SFML/Network.hpp>
#include <SFML/Network/Socket.hpp>
#include <SFML/System.hpp>
#include <stdexcept>
#include <vector>

bool HdlInputs_Classic(GameInstance &instance, std::vector<std::string> &cmd)
{
    int argc = cmd.size();
    sf::Packet packet;
    instance.getScreen().printPrompt();
    if (cmd[0] == "move" && argc < 2) {
        instance.getScreen().addToPrompt(
            "Invalid move. You must provide the room number", true);
        return false;
    }
    packet << ClientMessage::Type::PLAYERACTION;
    ClientMessage::PlayerAction action;
    if (instance.turns_in_prison > 0) {
        instance.prompt_user("You are stuck in jail for " +
                                 std::to_string(instance.turns_in_prison) +
                                 "laps :'( ",
                             std::vector<std::string>{""});
        return true;
    } else if (cmd[0] == "damage") {
        action.type = ClientMessage::PlayerAction::ActionType::DESTROY;
        action.data = instance._id_room;
    } else if (cmd[0] == "repair") {
        action.type = ClientMessage::PlayerAction::ActionType::REPAIR;
        action.data = instance._id_room;
    } else if (cmd[0] == "ask_vote") {
        action.type = ClientMessage::PlayerAction::ActionType::ASK_VOTE;
        action.data = instance._id_room;
    } else if (cmd[0] == "move") {
        action.type = ClientMessage::PlayerAction::ActionType::MOVE;
        try {
            action.data = std::stoi(cmd[1]);
        } catch (const std::invalid_argument &) {
            instance.getScreen().addToPrompt(
                "Invalid move. You must provide the room number", true);
            return false;
        }
    }
    packet << action;
    if (instance.getClient()._socket.send(packet) != sf::Socket::Done)
        throw std::runtime_error("Could not send action to server");
    return true;
}
