#include "Ship.hpp"
#include "config.hpp"
#include <exception>

Ship::Ship(std::string name) :
    _name(name), _rooms{NULL}
{
    for (int i = 0; i < 8; i++)
        _rooms[i] = new Room(room_names[i]);
}

Ship::~Ship(void)
{
    for (int i = 0; i < 8; i++)
        delete _rooms[i];
}

const std::string &Ship::getName(void) const
{
    return _name;
}

Room &Ship::getRoom(char index) const
{
    if (index < 0 || index > 7) {
        std::cerr << "Error in getRoom : Invalid index" << std::endl;
        throw std::exception();
    }
    return *(_rooms[index]);
}

Room &Ship::getRoom(const std::string &name) const
{
    for (auto room : _rooms)
        if (name == (*room).getName())
            return *room;
    std::cerr << "Error in getRoom : Invalid name" << std::endl;
    throw std::exception();
}

std::ostream &operator<<(std::ostream &s, Ship &ship)
{
    s << "SHIP: " << ship.getName() << " [" << std::endl;
    for (int i = 0; i < 7; i++)
        s << "--" << ship.getRoom(i) << std::endl;
    s << "]";
    return s;
}
