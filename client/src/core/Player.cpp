#include "Player.hpp"

Player::Player(std::string name, int id, Player::PlayerType type) :
    _name(name), _id(id), _type(type), _roundslocked(0), _currentRoom("None")
{
}

const std::string &Player::getName(void) const
{
    return _name;
}

Player::PlayerType Player::getType(void) const
{
    return _type;
}

int Player::getId(void) const
{
    return _id;
}

int Player::getRoundsLocked(void) const
{
    return _roundslocked;
}

const std::string &Player::getCurrentRoom(void) const
{
    return _currentRoom;
}

void Player::setCurrentRoom(const std::string &name)
{
    _currentRoom = name;
}

void Player::lock(int rounds)
{
    if (rounds >= 0)
        _roundslocked = rounds;
}

std::ostream &operator<<(std::ostream &s, const Player &player)
{
    s << "PLAYER: " << player.getName() << ", " \
      << ((player.getType() == Player::CREWMAN)? ("Crewman") : ("Traitor")) \
      << ", (Id:" << player.getId() << ") LOCK: " << player.getRoundsLocked() << " turns";
    return s;
}
