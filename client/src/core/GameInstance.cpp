#include "GameInstance.hpp"
#include "config.hpp"
#include "Client.hpp"
#include "config.h"

// CLASS METHODS

GameInstance::GameInstance(int port) :
    _currentRound(0), _maxRounds(nb_of_rounds), _stellarStormRound(-5), _alive(true), _is_voting(false),
    _ship(ship_name), _screen(), _players{}, _client(port), _self(nullptr)
{
    std::cerr << "Starting client on port " << port << std::endl;
}

int GameInstance::getCurrentRound(void) const
{
    return _currentRound;
}

int GameInstance::getMaxRounds(void) const
{
    return _maxRounds;
}

int GameInstance::getStellarStormRound(void) const
{
    return _stellarStormRound;
}

Ship &GameInstance::getShip(void)
{
    return _ship;
}

Screen &GameInstance::getScreen(void)
{
    return _screen;
}

Player &GameInstance::getPlayer(int index)
{
    return _players.at(index);
}

Client & GameInstance::getClient()
{
    return _client;
}

int GameInstance::getPlayerCount(void) const
{
    return _players.size();
}

std::vector<Player> & GameInstance::getPlayers()
{
    return _players;
}

std::vector<std::string> GameInstance::getPlayersName()
{
    std::vector<std::string> ret;
    std::transform(_players.cbegin(), _players.cend(), std::back_inserter(ret), [](const auto& p){ return p.getName(); });
    return ret;
}

bool GameInstance::getIsVoting()
{
    return _is_voting;
}

void GameInstance::addPlayer(Player &player)
{
    _players.push_back(player);
}

void GameInstance::setCurrentRound(int round)
{
    _currentRound = round;
}

void GameInstance::setStellarStormRound(int round)
{
    _stellarStormRound = round;
}

void GameInstance::setIsVoting(bool val)
{
    _is_voting = val;
}

bool GameInstance::isAlive(void) const
{
    return _alive;
}

void GameInstance::kill(void)
{
    _alive = false;
}

std::ostream &operator<<(std::ostream &s, GameInstance &instance)
{
    s << "GAME INSTANCE: Round " << instance.getCurrentRound() << " / " \
      << instance.getMaxRounds() << " Stellar storm: " << instance.getStellarStormRound()\
      << std::endl;
    s << instance.getShip() << std::endl;
    for (int i = 0; i < instance.getPlayerCount(); i++)
        std::cout << instance.getPlayer(i);
    return s;
}
