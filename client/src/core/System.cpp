#include "Communication.hpp"
#include "GameInstance.hpp"
#include "HdlInputs_Classic.hpp"
#include "Screen.hpp"
#include "config.hpp"
#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <string>

std::vector<std::string> GameInstance::prompt_user(
    const std::string &prompt, const std::vector<std::string> &actions)
{
    std::string cmd;
    std::vector<std::string> ret;
    Screen &screen = getScreen();

    while (true) {
        screen.addToPrompt(prompt);
        screen.printPrompt();
        cmd = screen.getPromptInput();
        if (cmd == "")
            continue;
        ret = parse_cmd(cmd);
        if (std::find(actions.begin(), actions.end(), ret[0]) != actions.end())
            return ret;
        screen.addToPrompt("Invalid command", true);
    }
}

void GameInstance::setup(void)
{
    std::string username;
    std::string ip;

    getScreen().drawMap();
    getScreen().addToPrompt("Insert server IP (localhost by default) :");
    getScreen().printPrompt();
    ip = getScreen().getPromptInput();
    if (ip.empty())
        ip = "localhost";
    getScreen().addToPrompt("Insert Username");
    getScreen().printPrompt();
    username = getScreen().getPromptInput();
    try {
        auto state = _client.connect(username, *this, ip);
        getScreen().printPlayerList(getPlayersName());
        getScreen().printTutorial(is_traitor);
        getScreen().printPlayerJob(is_traitor);
        _username = username;
        // getScreen().addToPrompt("Connected !");
        // getScreen().printPrompt();
        this->hdl_gamestate(state);
    } catch (std::runtime_error &e) {
        getScreen().addToPrompt(e.what());
        getScreen().printPrompt();
        getScreen().getPromptInput();
        endwin();
        kill();
    }
}

void GameInstance::update(void)
{
    auto player_name = getPlayersName();
    std::string player_listname;
    std::vector<std::string> cmd;
    if (turns_in_prison > 0) {
        getScreen().addToPrompt("You are in jail. You cannot do any actions :(",
                                true);
    } else if (_is_voting) {
        cmd = prompt_user("Vote for a player to jail: " + player_listname,
                          player_name);
        HdlInputs_Vote(*this, cmd);
    } else {
        do {
            cmd = prompt_user("Please chose an action:",
                              std::vector<std::string>{"move", "ask_vote",
                                                       "damage", "repair"});
        } while (!HdlInputs_Classic(*this, cmd));
    }
    check_new_state();
}

void GameInstance::do_round(void)
{
    static bool stellar_sleep = false;

    _currentRound++;
    if (!stellar_sleep)
        _stellarStormRound++;
    stellar_sleep = !(stellar_sleep);
    if (_currentRound == _maxRounds)
        crew_win();
    if (_stellarStormRound == _currentRound)
        traitors_win();
}

void GameInstance::traitors_win(void)
{
    kill();
    std::cout << "TRAITORS WON THE GAME" << std::endl;
}

void GameInstance::crew_win(void)
{
    kill();
    std::cout << "CREW_WIN" << std::endl;
}

void GameInstance::check_new_state()
{
    // getScreen().addToPrompt("Waiting for server response...");
    // getScreen().printPrompt();
    sf::Packet packet;
    if (_client._socket.receive(packet) != sf::Socket::Done)
        throw std::runtime_error("Could not receive response from server");
    ServerMessage::Type type;
    packet >> type;
    while (type == ServerMessage::LOG) {
        std::string log;
        packet >> log;
        getScreen().addToPrompt("SERVER: " + log);
        getScreen().printPrompt();
        if (auto status = _client._socket.receive(packet) != sf::Socket::Done)
            throw std::runtime_error("Could not receive response from server " +
                                     std::to_string(status));
        packet >> type;
    }
    if (type == ServerMessage::Type::RESULT) {
        ServerMessage::Result result;
        packet >> result;
        if (result == ServerMessage::Result::CREW_WIN) {
            _screen.addToPrompt("The crew won !");
        } else if (result == ServerMessage::Result::TRAITORS_WIN) {
            _screen.addToPrompt("The traitors won !");
        } else {
            _screen.addToPrompt("Nobody won ?");
        }
        _screen.addToPrompt("Press enter to exit");
        getScreen().printPrompt();
        getScreen().getPromptInput();
        kill();
    } else if (type == ServerMessage::GAME_STATE) {
        ServerMessage::GameState state;
        packet >> state;
        // getScreen().addToPrompt("Updating game status");
        // getScreen().printPrompt();
        hdl_gamestate(state);
    }
}

void GameInstance::hdl_gamestate(ServerMessage::GameState &stat)
{
    _id_room = stat.id_room;
    _is_voting = stat.voting;
    turns_in_prison = stat.turns_in_prison;
    getScreen().getMap().changePlayerPosition(stat.id_room);
    getScreen().printStatus(stat, false);
    getScreen().drawMap(false);
    getScreen().printPrompt(false);
    getScreen().refreshW();
}
