#include "Room.hpp"

Room::Room(std::string name) :
    _crashed(false), _health(100), _name(name)
{
}

bool Room::isCrashed(void) const
{
    return _crashed;
}

int Room::getHealth(void) const
{
    return _health;
}

const std::string &Room::getName(void) const
{
    return _name;
}

void Room::setHealth(int health)
{
    if (health > 100)
        health = 100;
    if (health < 0)
        health = 0;
    _health = health;
    _crashed = (_health == 0);
}

std::ostream &operator<<(std::ostream &s, const Room &room)
{
    s << "ROOM: " << room.getName() << " (" << room.getHealth() << "%) " \
      << ((room.isCrashed())? ("CRASHED") : ("NOT CRASHED"));
    return s;
}
