#include "Crewman.hpp"
#include "GameInstance.hpp"
#include "Player.hpp"
#include "Room.hpp"
#include "Ship.hpp"
#include "Traitor.hpp"
#include <exception>

int main(int ac, char **av)
{
    int port = SERVER_PORT;
    if (ac > 1)
        port = std::stoi(av[1]);

    try {
        GameInstance game(port);
        game.setup();
        while (game.isAlive()) {
            game.update();
        }
    } catch (const std::exception &e) {
        std::cerr << "Unexpected error: " << e.what() << std::endl;
    }
    return 0;
}
