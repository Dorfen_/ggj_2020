#include "Map.hpp"

Map::Map() :
    _map(),
    _coord{nullptr}
{}

Map::Map(const std::string &map) :
    _map(),
    _coord{nullptr}
{
    _coord[0] = new sf::Vector2u{10, 6};
    _coord[1] = new sf::Vector2u{33, 5};
    _coord[2] = new sf::Vector2u{66, 7};
    _coord[3] = new sf::Vector2u{93, 9};
    _coord[4] = new sf::Vector2u{14, 18};
    _coord[5] = new sf::Vector2u{52, 15};
    _coord[6] = new sf::Vector2u{82, 20};
    _coord[7] = new sf::Vector2u{109, 14};
    setMap(map);
}

Map::~Map()
{
    for (int i = 0; i < 8; i++)
        delete _coord[i];
}

const std::vector<std::string> &Map::getMap()const
{
    return _map;
}

bool Map::setMap(const std::string &map)
{
    std::ifstream map_file;

    map_file.open(map);
    if (map_file.is_open()) {
        std::string line;
        while (std::getline(map_file, line)) {
            _map.push_back(line);
        }
        map_file.close();
        return true;
    } else {
        std::cerr << "Unable to open file\n";
        return false;
    }
}

void Map::changePlayerPosition(const unsigned int index)
{
    if (index > _coord.size())
        throw std::logic_error("Invalid index");
    for (int i = 0; i < 8; i++)
        _map[_coord.at(i)->y][_coord.at(i)->x] = '$';
    _map[_coord.at(index)->y][_coord.at(index)->x] = 'P';
}
