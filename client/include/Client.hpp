#ifndef CLIENT_H
#define CLIENT_H

#include "server/include/Communication.hpp"
#include <SFML/Network.hpp>
#include <string>
class GameInstance;

class Client {
private:
    unsigned short _port;
public:
    sf::TcpSocket _socket;
    Client(const unsigned short port);
    void setIp(const std::string);
    ServerMessage::GameState connect(const std::string &username, GameInstance &instance, const std::string &ip);
};

#endif
