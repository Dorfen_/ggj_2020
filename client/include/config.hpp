#include <string>
#include <map>
#include <string>
#include <functional>
#include "GameInstance.hpp"

#ifndef CLIENT_CONFIG
#define CLIENT_CONFIG

// DEFAULT VALUES
const char *const room_names[8] = {"Labo", "Depot", "Cafeteria", "Maintenance Room",\
                                   "Hangar", "Mess", "Bedroom", "Cockpit"};
const int nb_of_rounds = 10;
const std::string ship_name = "USS Balkany";


// PLAYER ACTIONS
int hello_world(GameInstance &instance);

#endif
