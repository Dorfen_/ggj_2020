#include <string>
#include <iostream>
#include <iomanip>

#ifndef ROOM_CLASS
#define ROOM_CLASS

class Room {
    bool _crashed;
    int _health;
    const std::string _name;
public:
    Room(std::string name);
    bool isCrashed(void) const;
    int getHealth(void) const;
    const std::string &getName(void) const;
    void setHealth(int health);
};

std::ostream &operator<<(std::ostream &s, const Room &room);

#endif
