#include "Player.hpp"

#ifndef CREWMAN_CLASS
#define CREWMAN_CLASS

class Crewman : public Player {
public:
    Crewman(std::string name, int id);
};

#endif
