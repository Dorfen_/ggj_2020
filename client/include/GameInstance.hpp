#ifndef GAME_INSTANCE_CLASS
#define GAME_INSTANCE_CLASS

#include "Communication.hpp"
class GameInstance;

#include "Ship.hpp"
#include "Player.hpp"
#include "Client.hpp"
#include "Screen.hpp"
#include "Map.hpp"
#include <vector>
#include <map>
#include <functional>

class Client;

class GameInstance {
    // Attributes
    int _currentRound;
    const int _maxRounds;
    int _stellarStormRound;
    bool _alive;
    bool _is_voting;

    // Components
    Ship _ship;
    Screen _screen;
    std::vector<Player> _players;
    Client _client;
public:
    std::string _username = "";
    unsigned _id_room = 0;
    bool is_traitor = false;
    unsigned turns_in_prison = 0;

    // Litterally you
    Player *_self;
    // Class Methods
    GameInstance(int port);
    int getCurrentRound(void) const;
    int getMaxRounds(void) const;
    int getStellarStormRound(void) const;
    Ship &getShip(void);
    Screen &getScreen(void);
    Player &getPlayer(int index);
    Client &getClient();
    int getPlayerCount(void) const;
    std::vector<Player> &getPlayers();
    std::vector<std::string> getPlayersName();
    bool getIsVoting();
    void addPlayer(Player &player);
    void setCurrentRound(int round);
    void setStellarStormRound(int round);
    void setIsVoting(bool val);
    bool isAlive(void) const;
    void kill(void);

    // System Methods
    std::vector<std::string> prompt_user(const std::string &prompt, const std::vector<std::string> &actions);
    void setup(void);
    void update(void);
    void do_round(void);
    void traitors_win(void);
    void crew_win(void);

    // Get the response from the server after a turn is completed
    // Update the map or display end of game results
    void check_new_state();
    void hdl_gamestate(ServerMessage::GameState &stat);
};

std::ostream &operator<<(std::ostream &s, GameInstance &instance);

#endif
