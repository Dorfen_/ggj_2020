#include <iostream>
#include <iomanip>
#include <string>
#include <array>
#include "Room.hpp"

#ifndef SHIP_CLASS
#define SHIP_CLASS

class Ship {
    std::string _name;
    std::array<Room *, 8> _rooms;
public:
    Ship(std::string name);
    ~Ship(void);
    const std::string &getName(void) const;
    Room &getRoom(char index = 0) const;
    Room &getRoom(const std::string &name) const;
};

std::ostream &operator<<(std::ostream &s, Ship &ship);

#endif
