#include <ncurses.h>
#include <string>

#include "Map.hpp"
#include "Communication.hpp"
#include "Player.hpp"

#ifndef _DISPLAY_HPP_
#define _DISPLAY_HPP_

class Screen
{
    public:
        enum ScreenType {
            Help,
            Status,
            PlayerList,
            MapScreen,
            Prompt,
        };
        // Constructor
        Screen();
        // Destructor
        ~Screen();
        // Refresh all the WINDOWs
        void refreshW();
        // Refresh the selected WINDOW
        void refreshW(ScreenType type);
        // Prit the title of all the WINDOWs
        void titleW();
        // Prit the title of the selected WINDOW
        void titleW(ScreenType type);
        // Box all the WINDOWs
        void boxW();
        // Box the selected WINDOW
        void boxW(ScreenType type);
        // Print the map in the _map WINDOW
        void drawMap(bool r = true);
        // Ask for a str in the _prompt.
        std::string getPromptInput();
        // Refresh the promt
        void printPrompt(bool r = true);
        // Add a new line to the promt.
        void addToPrompt(const std::string &str, bool refresh = false);
        // getter for _Map
        Map &getMap();
        // print help message to _help
        void printTutorial(bool isTraitor, bool r = true);
        // print the player job
        void printPlayerJob(bool &isTraitor);
        // update the status window
        void printStatus(ServerMessage::GameState &status, bool r = true);
        // print the list of the player
        void printPlayerList(std::vector<std::string> player, bool r = true);
        // Init the ncurses screen
        static void initScreen();

    private:
        WINDOW *_help = nullptr;
        WINDOW *_map = nullptr;
        WINDOW *_prompt = nullptr;
        WINDOW *_playerList = nullptr;
        WINDOW *_status = nullptr;
        long unsigned int _prompt_size;
        std::vector<std::string> _prompt_history;
        Map _Map;
};

class ScreenErr: public std::exception
{
    public:
        ScreenErr(const std::string &message);
        const std::string getMessage()const;

    private:
        std::string _message;
};

#endif //_DISPLAY_HPP_
