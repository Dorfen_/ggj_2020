
#ifndef PLAYER_CLASS
#define PLAYER_CLASS

#include <string>
#include <iostream>
#include <iomanip>

class Player {
public:
    enum PlayerType { CREWMAN, TRAITOR, };
protected:
    const std::string _name;
    const int _id;
    const PlayerType _type;
    int _roundslocked;
    std::string _currentRoom;
public:
    Player(std::string name, int id, PlayerType type);
    const std::string &getName(void) const;
    PlayerType getType(void) const;
    int getId(void) const;
    int getRoundsLocked(void) const;
    const std::string &getCurrentRoom(void) const;
    void setCurrentRoom(const std::string &name);
    void lock(int rounds);
};

std::ostream &operator<<(std::ostream &s, const Player &player);

#endif
