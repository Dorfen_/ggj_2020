#include "Player.hpp"

#ifndef TRAITOR_CLASS
#define TRAITOR_CLASS

class Traitor : public Player {
public:
    Traitor(std::string name, int id);
};

#endif
