

#pragma once

#include "GameInstance.hpp"

bool HdlInputs_Classic(GameInstance &instance, std::vector<std::string> &cmd);
void HdlInputs_Vote(GameInstance &instance, std::vector<std::string> &cmd);

std::vector<std::string> parse_cmd(std::string cmd);
