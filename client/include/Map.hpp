#include <iostream>
#include <fstream>
#include <string>
#include <array>
#include <unordered_map>
#include <vector>
#include <SFML/System.hpp>

#ifndef _MAP_HEADER_HPP_
#define _MAP_HEADER_HPP_

class Map
{
    public:
        Map(const std::string &map);
        Map();
        ~Map();
        // load a map from a file
        bool setMap(const std::string &map);
        // getter for _map
        const std::vector<std::string> &getMap()const;
        // Set a new position for the player. (index of the room, username of the player)
        void changePlayerPosition(const unsigned int index);

    private:
        std::vector<std::string> _map;
        std::array<sf::Vector2u *, 8> _coord;
};

#endif //_MAP_HEADER_HPP_
