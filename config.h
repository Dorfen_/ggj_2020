#ifndef __CONFIG_H_
#define __CONFIG_H_

// Port listened to by the server
#define SERVER_PORT 46825

// Maximum number of player
#define PLAYER_NUMBER 5

// Number of traitors
#define TRAITORS_NUMBER 2

// Number of rooms
#define ROOM_NUMBER 8

// Traitors damages
#define TRAITORS_DAMAGES 50

// Crew repair
#define CREW_REPAIR_LEVEL 100

// Maximum number of turn
#define MAX_TURNS 15

// Number of turns spent in prison
#define TURNS_IN_PRISON 2

// Start distance of the storm
#define INITIAL_STORM_DISTANCE 5

#endif // __CONFIG_H_
