#include <SFML/Network.hpp>
#include <SFML/Network/Socket.hpp>
#include <iostream>
#include <stdexcept>

int main(int argc, char *argv[])
{
    if (argc != 2)
        throw std::invalid_argument("Usage: ./client IP_ADDRESS");
    sf::TcpSocket socket;
    sf::Socket::Status status = socket.connect(argv[1], 46825);
    if (status != sf::Socket::Done) {
        throw std::runtime_error("Could not connect");
    }

    socket.setBlocking(false);
    while (true) {
        sf::Packet packet;
        sf::Socket::Status status;
        while ((status = socket.receive(packet)) == sf::Socket::Partial);
        if (status == sf::Socket::NotReady)
            continue;
        if (status != sf::Socket::Done)
            break;
        std::cout << "Receiving packet" << std::endl;
        std::string line;
        packet >> line;
        std::cout << line << std::endl;
    }

    return 0;
}
