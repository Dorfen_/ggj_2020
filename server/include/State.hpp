#ifndef __STATE_H_
#define __STATE_H_

#include "Communication.hpp"
#include "config.h"
#include <array>
class ServerState
{
  public:
    ServerState();

    bool isTraitor(unsigned player) const;
    bool isInPrison(unsigned player) const;
    unsigned getPos(unsigned player) const;
    unsigned getHealth(unsigned room) const;
    bool isVoting() const;
    unsigned getTurn() const;
    std::array<bool, PLAYER_NUMBER> getPeopleInRoom(unsigned room) const;
    ServerMessage::GameState getGameState(unsigned player);
    bool isFinished() const;
    ServerMessage::Result getResult() const;
    unsigned getStormDistance() const;

    // Move a player to a new room. Consumes its action
    bool actionMove(unsigned player, unsigned room);
    // Repair the machine in the room the player is in. Consumes its action
    bool actionRepair(unsigned player, unsigned);
    // Destroy the machine in the room the player is in. Consumes its action
    bool actionDestroy(unsigned player, unsigned);
    // Asks for a vote next turn. Consumes the player's action
    bool actionAskVote(unsigned player, unsigned);

    // If a vote is happening, vote for the given player.
    // Consumes the voter's action
    bool actionVote(unsigned voter, unsigned target);

    // Actions table
    using action_t = bool (ServerState::*)(unsigned, unsigned);
    constexpr static const action_t actions[5] = {
        [ClientMessage::PlayerAction::MOVE] = &ServerState::actionMove,
        [ClientMessage::PlayerAction::REPAIR] = &ServerState::actionRepair,
        [ClientMessage::PlayerAction::DESTROY] = &ServerState::actionDestroy,
        [ClientMessage::PlayerAction::ASK_VOTE] = &ServerState::actionAskVote,
        [ClientMessage::PlayerAction::VOTE] = &ServerState::actionVote};

    // Move to the next turn
    bool nextTurn();

  private:
    unsigned _turn;
    // A vote will take place next turn
    bool _willVote;
    // A vote is currently taking place
    bool _voting;
    // Is the game finished
    ServerMessage::Result _result;
    // Distance from the solar storm
    unsigned _stormDistance;
    std::array<bool, PLAYER_NUMBER> _traitors;
    std::array<unsigned, PLAYER_NUMBER> _prisonTurns;
    std::array<unsigned, PLAYER_NUMBER> _playerPos;
    std::array<unsigned, PLAYER_NUMBER> _votes;
    std::array<bool, PLAYER_NUMBER> _hasPlayed;
    std::array<int, ROOM_NUMBER> _repairLevel;

};

#endif // __STATE_H_
