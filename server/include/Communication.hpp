
#pragma once

#include "config.h"
#include <SFML/Network.hpp>
#include <fstream>
#include <ios>
#include <string>

namespace ClientMessage
{
    enum Type : unsigned char { PLAYERINIT, PLAYERACTION };

    using PlayerInit = std::string;

    struct PlayerAction {
        enum ActionType : unsigned char {
            MOVE,
            REPAIR,
            DESTROY,
            ASK_VOTE,
            VOTE
        } type;
        unsigned data;
    };
    sf::Packet &operator>>(sf::Packet &p, PlayerAction &a);
    sf::Packet &operator<<(sf::Packet &p, PlayerAction &a);

    using Vote = unsigned;

    sf::Packet &operator>>(sf::Packet &p, Type &t);
    sf::Packet &operator<<(sf::Packet &p, Type t);
}

namespace ServerMessage
{
    enum Type : unsigned char { LOG, ERROR, GAME_STATE, LAUNCH, RESULT };

    using Log = std::string;
    using Error = std::string;

    struct GameState {
        unsigned id_room;
        unsigned char roomlife;
        std::array<bool, PLAYER_NUMBER> player;
        unsigned turns_in_prison;
        unsigned storm_distance;
        bool voting;
    };
    sf::Packet &operator>>(sf::Packet &p, GameState &g);
    sf::Packet &operator<<(sf::Packet &p, GameState &g);

    struct Launch {
        std::array<std::string, PLAYER_NUMBER> players;
        GameState state;
        bool is_traitor;
    };
    sf::Packet &operator>>(sf::Packet &p, Launch &l);
    sf::Packet &operator<<(sf::Packet &p, Launch &l);
    sf::Packet &operator>>(sf::Packet &p, Type &t);
    sf::Packet &operator<<(sf::Packet &p, Type t);

    enum Result : unsigned char { UNKNOWN, CREW_WIN, TRAITORS_WIN };
    sf::Packet &operator>>(sf::Packet &p, Result &l);
    sf::Packet &operator<<(sf::Packet &p, Result l);
}

inline void dump_packet(sf::Packet &packet)
{
    std::ofstream f("packet.log", std::ios_base::out | std::ios_base::app |
                                      std::ios_base::binary);
    auto size = packet.getDataSize();
    f << "NEW";
    f.write(static_cast<const char *>(packet.getData()), size);
}
