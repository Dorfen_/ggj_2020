#ifndef __SERVER_H_
#define __SERVER_H_

#include "Communication.hpp"
#include "State.hpp"
#include <SFML/Network/Packet.hpp>
#include <SFML/Network/TcpSocket.hpp>
#include <memory>
#include <string>

class Server
{
  public:
    Server(unsigned port, unsigned player_number);
    ~Server() = default;

    // Wait for all players to establish connections
    void get_connections();
    // Start the game
    void launch();

  private:
    unsigned _port;
    std::vector<std::unique_ptr<sf::TcpSocket>> _clients;
    std::array<std::string, PLAYER_NUMBER> _clientsName;
    unsigned _player_number;
    ServerState _state;

    // Send a message to all connected clients
    void sendMessage(const std::string &message);
    // Send a message to one connected client
    void sendMessageTo(unsigned client, const std::string &message);

    // Execute one request from a client
    void executeRequest(sf::Packet &packet, unsigned client_id);

    // Send the new game state to each client
    void sendGameState();

    // Sends the launch message to each client
    void sendLaunchMessage();

    // Sends the end game result to all players
    void sendEndResult();

    // Get player init request
    ClientMessage::PlayerInit getPlayerInit(sf::Packet &packet,
                                            std::size_t client_id);

    // Check if the game is ready to be launched
    bool isGameReady() const;
};

#endif // __SERVER_H_
