#include <SFML/Network.hpp>
#include <SFML/Network/Packet.hpp>
#include <SFML/Network/Socket.hpp>
#include <SFML/Network/TcpListener.hpp>
#include <SFML/Network/TcpSocket.hpp>
#include <iostream>
#include <stdexcept>
#include <string>
#include "Server.hpp"
#include "config.h"

using namespace std;

int main(int ac, char **av)
{
    int port = SERVER_PORT;
    if (ac > 1)
        port = std::stoi(av[1]);
    Server server(port, PLAYER_NUMBER);

    server.get_connections();
    server.launch();
    return 0;
}
