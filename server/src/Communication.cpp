#include "Communication.hpp"
#include "config.h"

namespace ClientMessage
{
    sf::Packet &operator>>(sf::Packet &p, PlayerAction &a)
    {
        unsigned char type;
        p >> type;
        a.type = static_cast<PlayerAction::ActionType>(type);
        p >> a.data;
        return p;
    }

    sf::Packet &operator<<(sf::Packet &p, PlayerAction &a)
    {
        p << static_cast<unsigned char>(a.type);
        p << a.data;
        return p;
    }

    sf::Packet &operator>>(sf::Packet &p, Type &t)
    {
        unsigned char tmp;
        p >> tmp;
        t = static_cast<Type>(tmp);
        return p;
    }

    sf::Packet &operator<<(sf::Packet &p, Type t)
    {
        auto tmp = static_cast<unsigned char>(t);
        p << tmp;
        return p;
    }
}

namespace ServerMessage
{
    sf::Packet &operator>>(sf::Packet &p, GameState &g)
    {
        p >> g.id_room;
        p >> g.roomlife;
        for (size_t i = 0; i < PLAYER_NUMBER; i++) {
            p >> g.player[i];
        }
        p >> g.turns_in_prison;
        p >> g.storm_distance;
        p >> g.voting;
        return p;
    }

    sf::Packet &operator<<(sf::Packet &p, GameState &g)
    {
        p << g.id_room;
        p << g.roomlife;
        for (size_t i = 0; i < PLAYER_NUMBER; i++) {
            p << g.player[i];
        }
        p << g.turns_in_prison;
        p << g.storm_distance;
        p << g.voting;
        return p;
    }

    sf::Packet &operator>>(sf::Packet &p, Launch &l)
    {
        for (size_t i = 0; i < PLAYER_NUMBER; i++)
            p >> l.players[i];
        p >> l.state;
        p >> l.is_traitor;
        return p;
    }

    sf::Packet &operator<<(sf::Packet &p, Launch &l)
    {
        for (size_t i = 0; i < PLAYER_NUMBER; i++)
            p << l.players[i];
        p << l.state;
        p << l.is_traitor;
        return p;
    }

    sf::Packet &operator>>(sf::Packet &p, Type &t)
    {
        unsigned char tmp;
        p >> tmp;
        t = static_cast<Type>(tmp);
        return p;
    }

    sf::Packet &operator<<(sf::Packet &p, Type t)
    {
        auto tmp = static_cast<unsigned char>(t);
        p << tmp;
        return p;
    }

    sf::Packet &operator>>(sf::Packet &p, Result &r)
    {
        unsigned char tmp;
        p >> tmp;
        r = static_cast<Result>(tmp);
        return p;
    }

    sf::Packet &operator<<(sf::Packet &p, Result r)
    {
        p << static_cast<unsigned char>(r);
        return p;
    }
}
