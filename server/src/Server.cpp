#include "Server.hpp"
#include "Communication.hpp"
#include "State.hpp"
#include "config.h"
#include <SFML/Network/Packet.hpp>
#include <SFML/Network/Socket.hpp>
#include <SFML/Network/SocketSelector.hpp>
#include <SFML/Network/TcpListener.hpp>
#include <SFML/Network/TcpSocket.hpp>
#include <algorithm>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>

// Debug: strings corresponding to possible player actions
constexpr static char const *action_type_str[5] = {"MOVE", "REPAIR", "DESTROY",
                                                   "ASK_VOTE", "VOTE"};

Server::Server(unsigned port, unsigned player_number)
    : _port(port),
      _clients(),
      _clientsName{},
      _player_number(player_number),
      _state()
{
    std::clog << "Starting server on port " << port << std::endl;
    _clients.reserve(player_number);
    std::fill(_clientsName.begin(), _clientsName.end(), "");
}

bool Server::isGameReady() const
{
    if (_clients.size() == _player_number) {
        for (auto &name : _clientsName)
            if (name.empty())
                return false;
        return true;
    } else {
        return false;
    }
}

void Server::get_connections()
{
    sf::TcpListener listener;
    sf::SocketSelector selector;

    if (listener.listen(_port) != sf::Socket::Done)
        throw std::runtime_error("Could not listen on port " +
                                 std::to_string(_port));
    selector.add(listener);

    while (!isGameReady()) {
        selector.wait();
        // Check that no clients disconnected, and get players initialisation
        for (size_t i = 0; i < _clients.size(); i++) {
            auto &client = *_clients[i];
            if (selector.isReady(client)) {
                sf::Packet packet;
                if (client.receive(packet) == sf::Socket::Disconnected) {
                    std::clog << "Client " << _clientsName[i] << " disconnected"
                              << std::endl;
                    _clients.erase(_clients.begin() + i);
                } else {
                    getPlayerInit(packet, i);
                }
            }
        }

        // Accept a new connection
        if (!selector.isReady(listener))
            continue;
        auto client = std::make_unique<sf::TcpSocket>();
        if (listener.accept(*client) != sf::Socket::Done)
            continue;
        selector.add(*client);
        std::clog << "New connection: " << client->getRemoteAddress()
                  << " Number: " << _clients.size() + 1 << std::endl;
        _clients.push_back(std::move(client));
        this->sendMessage(std::to_string(_clients.size()) + " players");
    }

    listener.close();
}

ClientMessage::PlayerInit Server::getPlayerInit(sf::Packet &packet,
                                                std::size_t client_id)
{
    ClientMessage::Type type;
    packet >> type;
    if (type != ClientMessage::PLAYERINIT)
        throw std::runtime_error("Invalid first packet from player");
    std::string name;
    packet >> name;
    _clientsName[client_id] = name;
    std::cerr << "Got initialization from client "
              << _clients[client_id]->getRemoteAddress() << " Name: " << name
              << std::endl;
    this->sendMessage("Player " + std::to_string(client_id) + " is called " +
                      name);
    return name;
}

void Server::launch()
{
    std::clog << "Launching server" << std::endl;
    this->sendMessage("Launching server");
    // Add all clients to a socket selector
    sf::SocketSelector selector;
    std::for_each(_clients.begin(), _clients.end(),
                  [&selector](auto &client) { selector.add(*client); });
    // Send the launch message to all clients
    this->sendLaunchMessage();

    // While the game is not finished
    while (_state.getTurn() < MAX_TURNS) {
        selector.wait();

        // Execute all available client requests
        for (size_t i = 0; i < _clients.size(); i++) {
            if (selector.isReady(*_clients[i])) {
                sf::Packet packet;
                sf::Socket::Status status;
                if ((status = _clients[i]->receive(packet)) == sf::Socket::Done)
                    this->executeRequest(packet, i);
                if (status == sf::Socket::Disconnected) {
                    std::clog << "Client '" << _clientsName[i]
                              << "' disconnected. Shutting down..."
                              << std::endl;
                    return;
                }
            }
        }

        // If the turn is finished, send back the new data
        if (_state.nextTurn()) {
            if (_state.isFinished()) {
                sendEndResult();
                return;
            } else
                sendGameState();
        }
    }
}

void Server::executeRequest(sf::Packet &packet, unsigned client_id)
{
    unsigned char type_c;
    packet >> type_c;
    ClientMessage::Type type = static_cast<ClientMessage::Type>(type_c);

    if (type == ClientMessage::PLAYERACTION) {
        ClientMessage::PlayerAction action;
        dump_packet(packet);
        packet >> action;
        std::cerr << "Received action " << action.type << " from player "
                  << _clientsName[client_id] << " with data " << action.data
                  << std::endl;
        if ((_state.*ServerState::actions[action.type])(client_id, action.data))
            sendMessageTo(client_id, "Action completed");
        else
            sendMessageTo(client_id, "Could not execute action");
    }
}

void Server::sendMessage(const std::string &message)
{
    std::clog << "Sending message: " << message << std::endl;
    for (auto &client : _clients) {
        sf::Packet packet;
        packet << static_cast<unsigned char>(ServerMessage::LOG) << message;
        if (client->send(packet) != sf::Socket::Done)
            std::clog << "Could not send message to client "
                      << client->getRemoteAddress() << std::endl;
    }
}

void Server::sendMessageTo(unsigned client, const std::string &message)
{
    std::clog << "Sending message: " << message << " to client: " << client
              << std::endl;
    sf::Packet packet;
    packet << static_cast<unsigned char>(ServerMessage::LOG) << message;
    if (_clients[client]->send(packet) != sf::Socket::Done)
        std::clog << "Could not send message to client "
                  << _clients[client]->getRemoteAddress() << std::endl;
}

void Server::sendGameState()
{
    for (size_t i = 0; i < _clients.size(); i++) {
        sf::Packet packet;
        packet << static_cast<unsigned char>(ServerMessage::GAME_STATE);
        auto state = _state.getGameState(i);
        packet << state;
        _clients[i]->send(packet);
    }
}

void Server::sendLaunchMessage()
{
    for (size_t i = 0; i < _clients.size(); i++) {
        sf::Packet packet;
        packet << static_cast<unsigned char>(ServerMessage::LAUNCH);
        ServerMessage::Launch launch;
        launch.players = _clientsName;
        launch.state = _state.getGameState(i);
        launch.is_traitor = _state.isTraitor(i);
        packet << launch;
        _clients[i]->send(packet);
    }
}

// Sends the end game result to all players
void Server::sendEndResult()
{
    std::clog << "Sending Results..." << std::endl;
    std::for_each(_clients.begin(), _clients.end(), [this](auto &client) {
        sf::Packet packet;
        packet << ServerMessage::RESULT;
        packet << this->_state.getResult();
        client->send(packet);
    });
}
