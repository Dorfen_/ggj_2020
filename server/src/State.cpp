#include "State.hpp"
#include "Communication.hpp"
#include "config.h"
#include <algorithm>
#include <cstdlib>
#include <iostream>

constexpr const ServerState::action_t ServerState::actions[5];

ServerState::ServerState()
    : _turn(0),
      _willVote(false),
      _voting(false),
      _result(ServerMessage::UNKNOWN),
      _stormDistance(INITIAL_STORM_DISTANCE),
      _traitors{false},
      _prisonTurns{0},
      _playerPos{0},
      _votes{0},
      _hasPlayed{false},
      _repairLevel()
{
    std::fill(_repairLevel.begin(), _repairLevel.end(), 100);
    std::fill_n(_traitors.begin(), TRAITORS_NUMBER, true);
    std::random_shuffle(_traitors.begin(), _traitors.end());
}

bool ServerState::isTraitor(unsigned player) const
{
    return _traitors.at(player);
}

bool ServerState::isInPrison(unsigned player) const
{
    return _prisonTurns.at(player) > 0;
}

unsigned ServerState::getPos(unsigned player) const
{
    return _playerPos.at(player);
}

unsigned ServerState::getHealth(unsigned room) const
{
    return _repairLevel.at(room);
}

bool ServerState::isVoting() const
{
    return _voting;
}

unsigned ServerState::getTurn() const
{
    return _turn;
}

std::array<bool, PLAYER_NUMBER> ServerState::getPeopleInRoom(
    unsigned room) const
{
    std::array<bool, PLAYER_NUMBER> res;
    std::transform(_playerPos.begin(), _playerPos.end(), res.begin(),
                   [room](unsigned pos) { return pos == room; });
    return res;
}

ServerMessage::GameState ServerState::getGameState(unsigned player)
{
    ServerMessage::GameState new_state;

    new_state.id_room = this->getPos(player);
    new_state.roomlife = this->getHealth(new_state.id_room);
    new_state.player = this->getPeopleInRoom(new_state.id_room);
    new_state.turns_in_prison = this->_prisonTurns[player];
    new_state.storm_distance = this->_stormDistance;
    new_state.voting = this->_voting;
    return new_state;
}

bool ServerState::isFinished() const
{
    return _result != ServerMessage::UNKNOWN;
}

ServerMessage::Result ServerState::getResult() const
{
    return _result;
}

unsigned ServerState::getStormDistance() const
{
    return _stormDistance;
}

// Move a player to a new room. Consumes its action
bool ServerState::actionMove(unsigned player, unsigned room)
{
    if (_hasPlayed.at(player) || _voting) {
        _hasPlayed.at(player) = true;
        return false;
    }
    _playerPos[player] = room;
    _hasPlayed.at(player) = true;
    return true;
}

// Repair the machine in the room the player is in. Consumes its action
bool ServerState::actionRepair(unsigned player, unsigned)
{
    if (_hasPlayed.at(player) || _voting) {
        _hasPlayed.at(player) = true;
        return false;
    }
    _repairLevel[_playerPos[player]] += CREW_REPAIR_LEVEL;
    if (_repairLevel[_playerPos[player]] > 100)
        _repairLevel[_playerPos[player]] = 100;
    _hasPlayed.at(player) = true;
    return true;
}

// Destroy the machine in the room the player is in. Consumes its action
bool ServerState::actionDestroy(unsigned player, unsigned)
{
    if (_hasPlayed.at(player) || !_traitors[player] || _voting) {
        _hasPlayed.at(player) = true;
        return false;
    }
    _repairLevel[_playerPos[player]] -= TRAITORS_DAMAGES;
    if (_repairLevel[_playerPos[player]] < 0)
        _repairLevel[_playerPos[player]] = 0;
    _hasPlayed.at(player) = true;
    return true;
}

// Asks for a vote next turn. Consumes the player's action
bool ServerState::actionAskVote(unsigned player, unsigned)
{
    if (_hasPlayed.at(player) || _voting) {
        _hasPlayed.at(player) = true;
        return false;
    }
    _willVote = true;
    _hasPlayed.at(player) = true;
    return true;
}

// If a vote is happening, vote for the given player.
// Consumes the voter's action
bool ServerState::actionVote(unsigned voter, unsigned target)
{
    if (_hasPlayed.at(voter) || !_voting) {
        _hasPlayed.at(voter) = true;
        return false;
    }

    _votes.at(target)++;
    _hasPlayed.at(voter) = true;
    return true;
}

// Move to the next turn
bool ServerState::nextTurn()
{
    // Do not end the turn if any player has not played
    if (std::any_of(_hasPlayed.begin(), _hasPlayed.end(),
                    [](bool played) { return played == false; }))
        return false;
    // Remove one turn of prison for each player in prison
    std::for_each(_prisonTurns.begin(), _prisonTurns.end(), [](auto &turns) {
        if (turns > 0)
            turns--;
    });
    // If a vote was happening, put the person with the most vote in prison
    if (_voting) {
        auto result = std::max_element(_votes.begin(), _votes.end());
        unsigned player = std::distance(_votes.begin(), result);
        _prisonTurns[player] = TURNS_IN_PRISON;
        _voting = false;
        std::fill(_votes.begin(), _votes.end(), 0);
    }
    // If a vote was asked, the next action must be a vote
    if (_willVote) {
        _willVote = false;
        _voting = true;
    }
    // If the turn is pair, let the storm approach
    if (_turn % 2 == 0)
        _stormDistance--;
    // If all systems are up, distance the storm
    if (std::all_of(_repairLevel.begin(), _repairLevel.end(),
                    [](auto &level) { return level > 0; }))
        _stormDistance++;
    // If the ship gets into the storm, the game is lost
    if (_stormDistance == 0 && _result == ServerMessage::UNKNOWN) {
        _result = ServerMessage::TRAITORS_WIN;
    }
    // Every player can play again next turn
    std::fill(_hasPlayed.begin(), _hasPlayed.end(), false);
    std::transform(_prisonTurns.begin(), _prisonTurns.end(), _hasPlayed.begin(),
                   [](auto turns) { return turns > 0; });
    // Move to next turn
    _turn++;
    // If the turn is the last turn, the crew win
    if (_turn >= MAX_TURNS && _result == ServerMessage::UNKNOWN)
        _result = ServerMessage::CREW_WIN;
    return true;
}
